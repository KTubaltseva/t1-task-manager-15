package ru.t1.ktubaltseva.tm.util;

import ru.t1.ktubaltseva.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextInt() {
        final String string = nextLine();
        try {
            return Integer.parseInt(string);
        } catch (final RuntimeException e) {
            throw new NumberIncorrectException(string, e);
        }
    }

}
