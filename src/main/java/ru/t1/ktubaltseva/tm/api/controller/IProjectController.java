package ru.t1.ktubaltseva.tm.api.controller;

import ru.t1.ktubaltseva.tm.exception.AbstractException;

public interface IProjectController {

    void clearProjects();

    void completeProjectById() throws AbstractException;

    void completeProjectByIndex() throws AbstractException;

    void changeProjectStatusById() throws AbstractException;

    void changeProjectStatusByIndex() throws AbstractException;

    void createProject() throws AbstractException;

    void displayProjectById() throws AbstractException;

    void displayProjectByIndex() throws AbstractException;

    void displayProjects();

    void displayProjectsFullInfo();

    void removeProjectById() throws AbstractException;

    void removeProjectByIndex() throws AbstractException;

    void startProjectById() throws AbstractException;

    void startProjectByIndex() throws AbstractException;

    void updateProjectById() throws AbstractException;

    void updateProjectByIndex() throws AbstractException;

}
