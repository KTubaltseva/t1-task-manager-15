package ru.t1.ktubaltseva.tm.api.controller;

import ru.t1.ktubaltseva.tm.exception.AbstractException;

public interface ITaskController {

    void clearTasks();

    void completeTaskById() throws AbstractException;

    void completeTaskByIndex() throws AbstractException;

    void changeTaskStatusById() throws AbstractException;

    void changeTaskStatusByIndex() throws AbstractException;

    void createTask() throws AbstractException;

    void displayTaskById() throws AbstractException;

    void displayTaskByIndex() throws AbstractException;

    void displayTasks();

    void displayTasksByProjectId() throws AbstractException;

    void displayTasksFullInfo();

    void removeTaskById() throws AbstractException;

    void removeTaskByIndex() throws AbstractException;

    void startTaskById() throws AbstractException;

    void startTaskByIndex() throws AbstractException;

    void updateTaskById() throws AbstractException;

    void updateTaskByIndex() throws AbstractException;

}
