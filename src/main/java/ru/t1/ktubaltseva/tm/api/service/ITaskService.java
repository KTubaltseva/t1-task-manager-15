package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(Task task) throws AbstractException;

    void clear();

    Task create(String name, String description) throws AbstractException;

    Task create(String name) throws AbstractException;

    Task changeTaskStatusById(String id, Status status) throws AbstractException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractException;

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId) throws AbstractException;

    Task findOneById(String id) throws AbstractException;

    Task findOneByIndex(Integer index) throws AbstractException;

    void remove(Task task) throws AbstractException;

    Task removeById(String id) throws AbstractException;

    Task removeByIndex(Integer Index) throws AbstractException;

    Task updateById(String id, String name, String description) throws AbstractException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractException;

}
