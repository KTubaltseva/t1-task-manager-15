package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String projectId, String taskId) throws AbstractException;

    Project removeProjectById(String projectId) throws AbstractException;

    Task unbindTaskFromProject(String projectId, String taskId) throws AbstractException;

}
