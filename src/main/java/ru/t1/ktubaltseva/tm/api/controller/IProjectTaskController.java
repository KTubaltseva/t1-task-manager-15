package ru.t1.ktubaltseva.tm.api.controller;

import ru.t1.ktubaltseva.tm.exception.AbstractException;

public interface IProjectTaskController {

    void bindTaskToProject() throws AbstractException;

    void unbindTaskFromProject() throws AbstractException;

}
